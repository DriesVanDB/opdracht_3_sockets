#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>

void SaveFile(int); /* function prototype */
void error(const char *msg)
{
    perror(msg);
    exit(1);
}

void *SigCatcher(int n)
{
  wait3(NULL,WNOHANG,NULL);
}

int main(int argc, char *argv[])
{
    int sockfd, newsockfd, portno, pid;
    socklen_t clilen;
    struct sockaddr_in serv_addr, cli_addr;

    if (argc < 2) {
        fprintf(stderr,"ERROR, no port provided \n");
        exit(1);
    }

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
         error("ERROR opening socket");
    }   
    bzero((char *) &serv_addr, sizeof(serv_addr));
    portno = (int) strtol(argv[2], (char **)NULL, 10);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){
        error("ERROR on binding");
    }        
    listen(sockfd,5);
    clilen = sizeof(cli_addr);
    signal(SIGCHLD,SIG_IGN);
    
    while (1) {
    
        newsockfd = accept(sockfd,(struct sockaddr *) &cli_addr, &clilen);
        if (newsockfd < 0){
            error("ERROR on accept\r\n");
        }       
        pid = fork();
        if (pid < 0)
            error("ERROR on fork\r\n");
        if (pid == 0)  {
            close(sockfd);
            SaveFile(newsockfd);
            exit(0);
        }
        else close(newsockfd);
    } 
    close(sockfd);
    return 0; 
}

void SaveFile (int sock)
{
    ssize_t len;
    char buffer[BUFSIZ];
    int file_size, n;
    int remain_data = 0;
    char* stringVariable;
    char* stringArray[2];
    int i = 0;
    FILE *file;
    char Naam[256];
    
    bzero(buffer,256);
    n = read(sock,buffer,255);
	
	//de extentie van de file eruit halen
	stringVariable = strtok (buffer,".");
    while (stringVariable != NULL)
    {
        stringArray[i] = stringVariable;
        stringVariable = strtok (NULL, "."); 
        i++;  
    }

    // de tijd 
    time_t current_time;
    char* c_time_string;

    current_time = time(NULL);


    //Convertern om ongedlige tekens uit de time format te halen 
    c_time_string = ctime(&current_time);
	
    strtok(c_time_string, "\r\n");

    bzero(Naam,256);
    strcat(Naam, c_time_string);
    strcat(Naam, ".");
    strcat(Naam, stringArray[1]);
	
	
    file = fopen(Naam, "w");
    if (file == NULL)
    {
		fprintf(stderr, "Failed to open file");
		exit(EXIT_FAILURE);
    }
    //laten weten dat de client mag zenden
    n = write(sock,"Start sending",18);

    recv(sock, buffer, BUFSIZ, 0);
    file_size = atoi(buffer); 
    remain_data = file_size;

    while (((len = recv(sock, buffer, BUFSIZ, 0)) > 0) && (remain_data > 0))
    {
            fwrite(buffer, sizeof(char), len, file);
            remain_data -= len;
            if(remain_data == 0 || remain_data <= 0){
                break;
            }
    }  
    printf("File recived \r\n");  
    fclose(file);

    n = write(sock,"File recived ",18);
    if (n < 0){
        error("ERROR writing to socket");
    }     
}

